//
//  intGA.cpp
//
//
//------------------------------------------------------------------------------
// Includes
//------------------------------------------------------------------------------
#include <iostream>
#include <cmath>
#include <fstream>
#include "intGA.h"
#include "process.h"
//#include "global.h"
#include "rand.h"

using namespace std;
extern std::ofstream outfile;
extern std::ofstream outfile_f_All;

//------------------------------------------------------------------------------
// PRNG Utility Functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Box-Muller method
double getRandomGaussian() 
{	
	double u1, u2, s;
	do
    {
		u1 = 2.0 * getRandomUniform() - 1.0;
		u2 = 2.0 * getRandomUniform() - 1.0;
		s = u1 * u1 + u2 * u2;
	}
	while (s >= 1.0);
	
	return u1 * sqrt(-2.0 * log(s) / s);
}



//------------------------------------------------------------------------------
// Int_IND Methods
//------------------------------------------------------------------------------

// initialize variables
// - num_genes  : the number of double values being evolved
// - maximize   : will fitness be maximized or minimized
Int_IND::Int_IND(int num_genes, bool maximize)
{	//outfile<<"In intGA.cpp: Int_IND::Int_IND;"<<"\n";
    m_numGenes = num_genes;
    m_fitness.resize(NUM_OBJECTIVE);
    m_evaluated = false;
    m_maximize = maximize;
    m_genes.resize(num_genes);

}

//------------------Copy from NewUI
 void Int_IND::copy_from(Int_IND *newUI){
	m_genes=newUI->getGenes();
	m_fitness= newUI->getFitness();
	rank=newUI->getRank();
	crowd_dist=newUI->get_crowd_dist();		
 }


//------------------------------------------------------------------------------
// mutate each double value with some probability
// - mute_rate : probability of mutating a single value
void Int_IND::mutateGenes(double rate, int min, int max)
{	//std::cout<<"In intGA.cpp: Int_IND::mutateGenes;"<<"\n";
    // mutate each locus
	for (int i = 0; i < m_numGenes; ++i)
    {
	if (getRandomUniform() <= rate)
        {
            // mutate value within range
            m_genes[i] = getRandomInt(min, max);
	}
    }
}

//------------------------------------------------------------------------------
// set the individual's genes
void Int_IND::setGenes(const vector<int> &new_genes)
{   //std::cout<<"In intGA.cpp: Int_IND::setGenes;"<<"\n";
    for (int i = 0; i < m_numGenes; ++i) m_genes[i] = new_genes[i];
}

//------------------------------------------------------------------------------
// randomize the individual's genes using a uniform distribution
void Int_IND::randomizeGenes(int min, int max)
{   // std::cout<<"In intGA.cpp: Int_IND::randomizeGenes;"<<"\n";
	for (int i = 0; i < m_numGenes; ++i) m_genes[i] = getRandomInt(min, max);
}

//------------------------------------------------------------------------------
// return a new individual that is created from single point crossover
// other : the second crossover individual
void Int_IND::crossoverSP(const Int_IND *other, Int_IND *newUI_1, Int_IND *newUI_2)
{
    // select the crossover point
    int cp = getRandomInt(0, m_numGenes-1);
    
    //
    // set the new individuals genes
    //      
    // first, from this individual
    for (int i = 0; i < cp; ++i)
    {
        newUI_1->m_genes[i] = this->m_genes[i];
        newUI_2->m_genes[i] = other->m_genes[i];
    }
        
    // next, from other individual
    for (int i = cp; i < m_numGenes; ++i)
    {
        newUI_1->m_genes[i] = other->m_genes[i];
        newUI_2->m_genes[i] = this->m_genes[i];
    }

}

//------------------------------------------------------------------------------
// set the fitness of this individual
// fit : value (higher = more fit)
void Int_IND::setFitness(const vector<double> &fit)
{	//std::cout<<"In intGA.cpp: Int_IND::setFitness;"<<"\n";
    for (int i = 0; i < NUM_OBJECTIVE; ++i) m_fitness[i] = fit[i];
    // m_fitness = fit;
    m_evaluated = true;
}

//------------------------------------------------------------------------------
// return the fitness of this individual, only valid if evaluated
const vector<double> Int_IND::getFitness()
{	//std::cout<<"In intGA.cpp: Int_IND::getFitness;"<<"\n";
   
    return m_fitness;
}


//------------------------------------------------------------------------------
// Int_GA Methods
//------------------------------------------------------------------------------

// setup the genetic algorithm
Int_GA::Int_GA(const PopulationInfo& info)
{	//std::cout<<"In intGA.cpp: Int_GA::Int_GA;"<<"\n";
    // initialize evolution parameters
	m_popSize = info.pop_size;
    m_subSize = info.sub_size;
    m_genomeSize = info.genome_length;
    m_geneMax = info.gene_max;
    m_geneMin = info.gene_min;
    m_numGen = info.num_generations;
    m_tournSize = info.tournament_size;
    m_elitism = info.elitism;
    m_muteProb = info.mutation_rate;
	m_fitnessFunc = info.calculate_fitness;
    m_curGen = 0;
	maximize=info.maximize;
    
    // randomly initialize population
    m_individuals.resize(m_popSize);
    archive_individuals.resize(m_popSize);
    
    for (int i = 0; i < m_popSize; ++i)
    {
        m_individuals[i] = new Int_IND(m_genomeSize, info.maximize);
        archive_individuals[i] = new Int_IND(m_genomeSize, info.maximize);
		m_individuals[i]->randomizeGenes(m_geneMin, m_geneMax);
	}
	
}

//------------------------------------------------------------------------------
// destroy the population
Int_GA::~Int_GA()
{	//std::cout<<"In intGA.cpp: Int_GA::~Int_GA();"<<"\n";
	for (int i = 0; i < m_popSize; ++i) {
       delete m_individuals[i];
       delete archive_individuals[i];
    }
}

//------------------------------------------------------------------------------
// select an individual using tournament selection

Int_IND* Int_GA::tournament (Int_IND* ind1, Int_IND* ind2)
{
    int flag;
    flag = check_dominance (ind1->getFitness(), ind2->getFitness(), NUM_OBJECTIVE);
    if (flag==1)
    {
        return (ind1);
    }
    if (flag==-1)
    {
        return (ind2);
    }
    if (ind1->get_crowd_dist() > ind2->get_crowd_dist())
    {
        return(ind1);
    }
    if (ind2->get_crowd_dist() > ind1->get_crowd_dist())
    {
        return(ind2);
    }
    if ((randomperc()) <= 0.5)
    {
        return(ind1);
    }
    else
    {
        return(ind2);
    }
}


//------------------------------------------------------------------------------
// create the next generation
void Int_GA::reproduce()
{
    int i, j,ii;
    
    if(getCurGen()==0){
        cout<<"0st generation"<<endl;
        //IF it is initial generation
        assign_rank_and_crowding_distance(m_individuals);
		for ( i = 0; i < m_popSize; ++i){
			archive_individuals[i]->copy_from(m_individuals[i]);
		}
    
    }else{
        cout<<getCurGen()<<"th generation"<<endl;
        //From 2nd generation:
        //merge archive_individuals and m_individuals
        //Put the half good to archive_individuals
        vector<Int_IND*> mix_pop(m_popSize*2);
		for( j=0;j<m_popSize*2;j++){
			mix_pop[j] = new Int_IND(m_genomeSize, maximize);
		}
        merge_pop(mix_pop);
        fill_nondominated_sort(mix_pop);
    }
   
	//print_Gnomes();

    int one_ind, two_ind, temp_randInt,one_ind_2, two_ind_2;
    Int_IND *parent1, *parent2;
	vector<double> fitness;
    
    // create space for new individuals
   // vector<Int_IND*> newIndividuals(m_popSize);

    // new individuals are created from current population
    for ( i = 0; i < m_popSize; ++i)
    {    
	 fitness = m_individuals[i]->getFitness();

        // create replacement individual if this individual is not elite
    }


    
    for ( i = 0; i < m_popSize; ++i)
    {
        fitness = archive_individuals[i]->getFitness();
        for( ii=0; ii<NUM_OBJECTIVE-1;ii++){
        outfile_f_All<<fitness[ii]<<", ";
        }
        outfile_f_All<<fitness[ii]<<endl;

    }
    

    
    // Reproduce m_individuals from selecting, crossover, mutation
    selection();
    
    for( i = 0; i < m_popSize; i=i+2){
      
        m_individuals[i]->mutateGenes(m_muteProb, m_geneMin, m_geneMax);
        m_individuals[i+1]->mutateGenes(m_muteProb, m_geneMin, m_geneMax);
        
    }
    

	//print_Gnomes();
      
}

//------------------------------------------------------------------------------
// evaluate every individual
// user must provide the fitness function
void Int_GA::evaluate()
{	//std::cout<<"In intGA.cpp: Int_GA::evaluate;"<<"\n";
	for (int i = 0; i < m_popSize; ++i)
    {
		(*m_fitnessFunc)(m_individuals[i]);
        if(getCurGen()>0){
            (*m_fitnessFunc)(archive_individuals[i]);
        }
	}
}

//------------------------------------------------------------------------------
// generate the next generation
bool Int_GA::nextGen()
{	//std::cout<<"In intGA.cpp: Int_GA::nextGen;"<<"\n";
    ++m_curGen;
    return isComplete();
}

//------------------------------------------------------------------------------
/* Function to assign rank and crowding distance to a population of size pop_size*/
void Int_GA:: assign_rank_and_crowding_distance (vector<Int_IND*> m_pop)
{
    int flag;
    int i;
    int end;
    int front_size;
    int rank=1;
    list *orig;
    list *cur;
    list *temp1, *temp2;
    orig = (list *)malloc(sizeof(list));
    cur = (list *)malloc(sizeof(list));
    front_size = 0;
    orig->index = -1;
    orig->parent = NULL;
    orig->child = NULL;
    cur->index = -1;
    cur->parent = NULL;
    cur->child = NULL;
    temp1 = orig;
    for (i=0; i<m_popSize; i++)
    {
        insert (temp1,i);
        temp1 = temp1->child;
    }
    do
    {
        if (orig->child->child == NULL)
        {
            m_pop[orig->child->index]->setRank(rank);
            m_pop[orig->child->index]->set_crowd_dist(INF);

            break;
        }
        temp1 = orig->child;
        insert (cur, temp1->index);
        front_size = 1;
        temp2 = cur->child;
        temp1 = del (temp1);
        temp1 = temp1->child;
        do
        {
            temp2 = cur->child;
            do
            {
                end = 0;
                flag = check_dominance (m_pop[temp1->index]->getFitness(), m_pop[temp2->index]->getFitness(),NUM_OBJECTIVE);
                if (flag == 1)
                {
                    insert (orig, temp2->index);
                    temp2 = del (temp2);
                    front_size--;
                    temp2 = temp2->child;
                }
                if (flag == 0)
                {
                    temp2 = temp2->child;
                }
                if (flag == -1)
                {
                    end = 1;
                }
            }
            while (end!=1 && temp2!=NULL);
            if (flag == 0 || flag == 1)
            {
                insert (cur, temp1->index);
                front_size++;
                temp1 = del (temp1);
            }
            temp1 = temp1->child;
        }
        while (temp1 != NULL);
        temp2 = cur->child;
        do
        {
            m_pop[temp2->index]->setRank(rank);
            temp2 = temp2->child;
        }
        while (temp2 != NULL);
        assign_crowding_distance_list (m_pop, cur->child, front_size);
        temp2 = cur->child;
        do
        {
            temp2 = del (temp2);
            temp2 = temp2->child;
        }
        while (cur->child !=NULL);
        rank+=1;
    }
    while (orig->child!=NULL);
    free (orig);
    free (cur);
    return;
}


/* Routine to compute crowding distance based on ojbective function values when the population in in the form of a list */
void Int_GA::assign_crowding_distance_list  (vector<Int_IND*> m_pop, list *lst, int front_size)
{
    int **obj_array;
    int *dist;
    int i, j;
    list *temp;
    temp = lst;
    if (front_size==1)
    {
        m_pop[lst->index]->set_crowd_dist(INF);
        return;
    }
    if (front_size==2)
    {
        m_pop[lst->index]->set_crowd_dist(INF);
        m_pop[lst->child->index]->set_crowd_dist(INF);
        return;
    }
    obj_array = (int **)malloc(NUM_OBJECTIVE*sizeof(int*));
    dist = (int *)malloc(front_size*sizeof(int));
    for (i=0; i<NUM_OBJECTIVE; i++)
    {
        obj_array[i] = (int *)malloc(front_size*sizeof(int));
    }
    for (j=0; j<front_size; j++)
    {
        dist[j] = temp->index;
        temp = temp->child;
    }
    assign_crowding_distance (m_pop, dist, obj_array, front_size);
    free (dist);
    for (i=0; i<NUM_OBJECTIVE; i++)
    {
        free (obj_array[i]);
    }
    //free (obj_array);

    return;
}

/* Routine to compute crowding distance based on objective function values when the population in in the form of an array */
void Int_GA::assign_crowding_distance_indices (vector<Int_IND*> m_pop, int c1, int c2)
{
    int **obj_array;
    int *dist;
    int i, j;
    int front_size;
    front_size = c2-c1+1;
    if (front_size==1)
    {
        m_pop[c1]->set_crowd_dist(INF);
        return;
    }
    if (front_size==2)
    {
        m_pop[c1]->set_crowd_dist(INF);
        m_pop[c2]->set_crowd_dist(INF);
        return;
    }
    obj_array = (int **)malloc(NUM_OBJECTIVE*sizeof(int*));
    dist = (int *)malloc(front_size*sizeof(int));
    for (i=0; i<NUM_OBJECTIVE; i++)
    {
        obj_array[i] = (int *)malloc(front_size*sizeof(int));
    }
    for (j=0; j<front_size; j++)
    {
        dist[j] = c1++;
    }
    assign_crowding_distance (m_pop, dist, obj_array, front_size);
    free (dist);
    for (i=0; i<NUM_OBJECTIVE; i++)
    {
        free (obj_array[i]);
    }
    free (obj_array);
    return;
}

/* Routine to compute crowding distances */
void Int_GA::assign_crowding_distance (vector<Int_IND*> m_pop, int *dist, int **obj_array, int front_size)
{
    int i, j;
    for (i=0; i<NUM_OBJECTIVE; i++)
    {
        for (j=0; j<front_size; j++)
        {
            obj_array[i][j] = dist[j];
        }
        quicksort_front_obj (m_pop, i, obj_array[i], front_size);
    }
    for (j=0; j<front_size; j++)
    {
        m_pop[dist[j]]->set_crowd_dist(0.0);
    }
    for (i=0; i<NUM_OBJECTIVE; i++)
    {
        m_pop[obj_array[i][0]]->set_crowd_dist(INF);
    }
    for (i=0; i<NUM_OBJECTIVE; i++)
    {
        for (j=1; j<front_size-1; j++)
        {
            if (m_pop[obj_array[i][j]]->get_crowd_dist() != INF)
            {
                if (m_pop[obj_array[i][front_size-1]]->getOneFitness(i)== m_pop[obj_array[i][0]]->getOneFitness(i))
                {
                    m_pop[obj_array[i][j]]->set_crowd_dist(m_pop[obj_array[i][j]]->get_crowd_dist()+0.0);
                }
                else
                {
                    m_pop[obj_array[i][j]]->set_crowd_dist( m_pop[obj_array[i][j]]->get_crowd_dist() +(m_pop[obj_array[i][j+1]]->getOneFitness(i) - m_pop[obj_array[i][j-1]]->getOneFitness(i))/(m_pop[obj_array[i][front_size-1]]->getOneFitness(i) - m_pop[obj_array[i][0]]->getOneFitness(i)));
                }
            }
        }
    }
    for (j=0; j<front_size; j++)
    {
        if (m_pop[dist[j]]->get_crowd_dist() != INF)
        {
            m_pop[dist[j]]->set_crowd_dist( (m_pop[dist[j]]->get_crowd_dist())/NUM_OBJECTIVE);
        }
    }
    return;
}


/* Randomized quick sort routine to sort a population based on a particular objective chosen */
void Int_GA::quicksort_front_obj(vector<Int_IND*> m_pop, int objcount, int obj_array[], int obj_array_size)
{
    q_sort_front_obj (m_pop, objcount, obj_array, 0, obj_array_size-1);
    return;
}

/* Actual implementation of the randomized quick sort used to sort a population based on a particular objective chosen */
void Int_GA::q_sort_front_obj(vector<Int_IND*> m_pop, int objcount, int obj_array[], int left, int right)
{
    int index;
    int temp;
    int i, j;
    double pivot;
    if (left<right)
    {
        index = rnd (left, right);
        temp = obj_array[right];
        obj_array[right] = obj_array[index];
        obj_array[index] = temp;
        pivot = m_pop[obj_array[right]]->getOneFitness(objcount);
        i = left-1;
        for (j=left; j<right; j++)
        {
            if (m_pop[obj_array[j]]->getOneFitness(objcount) <= pivot)
            {
                i+=1;
                temp = obj_array[j];
                obj_array[j] = obj_array[i];
                obj_array[i] = temp;
            }
        }
        index=i+1;
        temp = obj_array[index];
        obj_array[index] = obj_array[right];
        obj_array[right] = temp;
        q_sort_front_obj (m_pop, objcount, obj_array, left, index-1);
        q_sort_front_obj (m_pop, objcount, obj_array, index+1, right);
    }
    return;
}

/* Randomized quick sort routine to sort a population based on crowding distance */
void Int_GA::quicksort_dist(vector<Int_IND*> m_pop, int *dist, int front_size)
{
    q_sort_dist (m_pop, dist, 0, front_size-1);
    return;
}

/* Actual implementation of the randomized quick sort used to sort a population based on crowding distance */
void Int_GA::q_sort_dist(vector<Int_IND*> m_pop, int *dist, int left, int right)
{
    int index;
    int temp;
    int i, j;
    double pivot;
    if (left<right)
    {
        index = rnd (left, right);
        temp = dist[right];
        dist[right] = dist[index];
        dist[index] = temp;
        pivot = m_pop[dist[right]]->get_crowd_dist();
        i = left-1;
        for (j=left; j<right; j++)
        {
            if (m_pop[dist[j]]->get_crowd_dist()<= pivot)
            {
                i+=1;
                temp = dist[j];
                dist[j] = dist[i];
                dist[i] = temp;
            }
        }
        index=i+1;
        temp = dist[index];
        dist[index] = dist[right];
        dist[right] = temp;
        q_sort_dist (m_pop, dist, left, index-1);
        q_sort_dist (m_pop, dist, index+1, right);
    }
    return;
}


/* Routine to merge two populations into one */
void Int_GA::merge_pop(vector<Int_IND*> mix_population){
    for (int i=0; i<m_popSize; i++){
		mix_population[i]->copy_from(m_individuals[i]);
        mix_population[i+m_popSize]->copy_from(archive_individuals[i]);
    }
}


/* Routine to perform non-dominated sorting */
void Int_GA::fill_nondominated_sort (vector<Int_IND*> mixed_pop)
{//new_pop is archive
    int flag;
    int i, j;
    int end;
    int front_size;
    int archieve_size;
    int rank=1;
    list *pool;
    list *elite;
    list *temp1, *temp2;
    pool = (list *)malloc(sizeof(list));
    elite = (list *)malloc(sizeof(list));
    front_size = 0;
    archieve_size=0;
    pool->index = -1;
    pool->parent = NULL;
    pool->child = NULL;
    elite->index = -1;
    elite->parent = NULL;
    elite->child = NULL;
    temp1 = pool;
    for (i=0; i<2*m_popSize; i++)
    {
        insert (temp1,i);
        temp1 = temp1->child;
    }
    i=0;
    do
    {
        temp1 = pool->child;
        insert (elite, temp1->index);
        front_size = 1;
        temp2 = elite->child;
        temp1 = del (temp1);
        temp1 = temp1->child;
        do
        {
            temp2 = elite->child;
            if (temp1==NULL)
            {
                break;
            }
            do
            {
                end = 0;
                flag = check_dominance(mixed_pop[temp1->index]->getFitness(), mixed_pop[temp2->index]->getFitness(), NUM_OBJECTIVE);
                if (flag == 1)
                {
                    insert (pool, temp2->index);
                    temp2 = del (temp2);
                    front_size--;
                    temp2 = temp2->child;
                }
                if (flag == 0)
                {
                    temp2 = temp2->child;
                }
                if (flag == -1)
                {
                    end = 1;
                }
            }
            while (end!=1 && temp2!=NULL);
            if (flag == 0 || flag == 1)
            {
                insert (elite, temp1->index);
                front_size++;
                temp1 = del (temp1);
            }
            temp1 = temp1->child;
        }
        while (temp1 != NULL);
        temp2 = elite->child;
        j=i;
        if ( (archieve_size+front_size) <= m_popSize)
        {
            do
            {
                
				archive_individuals[i]->copy_from(mixed_pop[temp2->index]);
				archive_individuals[i]->setRank(rank);

                archieve_size+=1;
                temp2 = temp2->child;
                i+=1;
            }
            while (temp2 != NULL);
            assign_crowding_distance_indices (archive_individuals, j, i-1);
            rank+=1;
        }
        else
        {
            crowding_fill (mixed_pop, archive_individuals, i, front_size, elite);
            archieve_size = m_popSize;
            for (j=i; j<m_popSize; j++)
            {
                archive_individuals[j]->setRank(rank);
            }
        }
        temp2 = elite->child;
        do
        {
            temp2 = del (temp2);
            temp2 = temp2->child;
        }
        while (elite->child !=NULL);
    }
    while (archieve_size < m_popSize);
    while (pool!=NULL)
    {
        temp1 = pool;
        pool = pool->child;
        free (temp1);
    }
    while (elite!=NULL)
    {
        temp1 = elite;
        elite = elite->child;
        free (temp1);
    }
    return;
}

/* Routine to fill a population with individuals in the decreasing order of crowding distance */
void Int_GA::crowding_fill (vector<Int_IND*> mixed_pop, vector<Int_IND*> new_pop, int count, int front_size, list *elite)
{
    int *dist;
    list *temp;
    int i, j;
    assign_crowding_distance_list (mixed_pop, elite->child, front_size);
    dist = (int *)malloc(front_size*sizeof(int));
    temp = elite->child;
    for (j=0; j<front_size; j++)
    {
        dist[j] = temp->index;
        temp = temp->child;
    }
    quicksort_dist (mixed_pop, dist, front_size);
    for (i=count, j=front_size-1; i<m_popSize; i++, j--)
    {
		new_pop[i]->copy_from(mixed_pop[dist[j]]); 
	}

    free (dist);
    return;
}

/* Routine for tournament selection, it creates a new_pop from old_pop by performing tournament selection and the crossover */
void Int_GA::selection(){
    vector<double> fitness_val;
    vector<int> genome;
    int temp;
    int i;
    int rand;
    Int_IND *parent1, *parent2;
	vector<int> a1(m_popSize,0);
	vector<int> a2(m_popSize,0);
   
    for (i=0; i<m_popSize; i++)
    {
        a1[i] =  i;
		a2[i] = i;
    }
    for (i=0; i<m_popSize; i++)
    {
        rand = rnd (i, m_popSize-1);
        temp = a1[rand];
        a1[rand] = a1[i];
        a1[i] = temp;
        rand = rnd (i, m_popSize-1);
        temp = a2[rand];
        a2[rand] = a2[i];
        a2[i] = temp;
    }
    for (i=0; i<m_popSize; i+=4)
    {
      
        
        parent1 = tournament (archive_individuals[a1[i]], archive_individuals[a1[i+1]]);
        
        parent2 = tournament (archive_individuals[a1[i+2]], archive_individuals[a1[i+3]]);
        
		 
        
        parent1->crossoverSP(parent2, m_individuals[i], m_individuals[i+1]);
        parent1 = tournament (archive_individuals[a2[i]], archive_individuals[a2[i+1]]);
        
        

        parent2 = tournament (archive_individuals[a2[i+2]], archive_individuals[a2[i+3]]);
  
        parent1->crossoverSP(parent2, m_individuals[i+2], m_individuals[i+3]);
        

    }

    return;
}

void Int_GA::print_Gnomes(){
//print
}