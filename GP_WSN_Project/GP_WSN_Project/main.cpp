//
//  main.cpp
//

//------------------------------------------------------------------------------
// Includes
//------------------------------------------------------------------------------
#include <sstream>
#include <iostream>
#include <iomanip>
#include "process.h"
#include "intGA.h"
#include <fstream> 
#include "global.h"
#include "rand.h"



using   std::vector;
using namespace std;





//------------------------------------------------------------------------------
// Prototypes
//------------------------------------------------------------------------------
void evaluate_individual(Int_IND* individual);          // calculate objective function

std::ofstream outfile("GA_output.txt",ios_base::out);
std::ofstream outfile_f_All("GA_objective_all.csv",ios_base::out);


//------------------------------------------------------------------------------
// Main
//------------------------------------------------------------------------------
int main (int argc, const char * argv[])
{	double program_start_time=time(NULL);
	
    time_t time_seed=time(NULL);
	srand(time_seed);
    getRandomUniform();
    getRandomUniform();
    
	seed=getRandomUniform();
	randomize();

    //
    // setup GA
    //
    PopulationInfo pi;
    pi.pop_size = 100;
    pi.sub_size = pi.pop_size;
    pi.genome_length = NUM_INST * NUM_PROC;
    pi.gene_max = NUM_ACTIONS-1;
    pi.gene_min = 0;
    pi.num_generations = 100;
    pi.tournament_size = 3;
    pi.elitism = true;
    pi.mutation_rate = 3./(double)pi.genome_length;
    pi.maximize = true;
    pi.calculate_fitness = evaluate_individual;
    Int_GA ga(pi);
	
	outfile<<"Number of total Rounds: "<<NUM_LOOP<<endl;
	outfile<<"Pop size: "<<pi.pop_size<<", Testcase size: "<<NUM_LOOP<<endl;
 	outfile<<"tournament_size:"<< pi.tournament_size<<endl;
    outfile<<"mutation_rate: "<<pi.mutation_rate<<", genome_length: "<<pi.genome_length<<endl; 
    outfile<<"NSGAII"<<"\n";
	outfile<<"Time Random Seed:"<<time_seed<<", double seed:"<<seed<<endl;
    outfile_f_All<<"Gen1"<<endl;
    //
    // run the GA
    //
	// MRT
    do
    {
        // evaluate all individuals
        ga.evaluate();
        
        // print metrics
		outfile<<endl<<endl<<"******************************************************************"<<endl;
        outfile<< "Generation:"<<ga.getCurGen() << ":\t";
        cout<<"Generation:"<< ga.getCurGen() << ":\t";
        cout<< "Elasped time:" << time(NULL)-program_start_time<<";\t\t\t";
        
		outfile<< "Elasped time:" << time(NULL)-program_start_time<<";\t\t\t";
        
        // generate next generation
        if (ga.getCurGen() < (pi.num_generations-1))
        {
            ga.reproduce();
        }
        
    } while (!ga.nextGen());
	

	/*
	//test
	Int_IND indv(NUM_INST * NUM_PROC, true);
	vector<int> sequence; 
	sequence.resize(NUM_INST * NUM_PROC);

	sequence[0] = TRANSMIT; 
	sequence[1] = TRANSMIT;
	sequence[2] = SLEEP;

	sequence[3] = LISTEN;
	sequence[4] = TRANSMIT;
	sequence[5] = LISTEN;

	sequence[6] = LISTEN;
	sequence[7] = LISTEN;
	sequence[8] = TRANSMIT;

	indv.setGenes(sequence);
	evaluate_individual(&indv);
    */
    return 0;
}


//------------------------------------------------------------------------------
// Function Implementations
//------------------------------------------------------------------------------
 
//------------------------------------------------------------------------------
void evaluate_individual(Int_IND* individual)
{
	int total_transmited = 0; //MRT: total transmited sucessfully 

    vector<double> objective;
    int i,j,n;
    
    // decode genomes into action sequence
    vector<int> sequences;
    sequences=individual->getGenes();
    
	//Run the program in sync mode
	SinkNode BS;
    NormalNode sensorNode[NUM_PROC-1];
    
	
	//Setup all the node, and initialize them
	for (i = 0; i<NUM_PROC-1; i++){// set sid, nbr, and actions
		if (i == 0){
			if (NUM_PROC > 2) sensorNode[i].reset(&BS, &sensorNode[i+1], sequences, i+1);
			else sensorNode[i].reset(&BS, NULL, sequences, i+1);
		}
		else if (i == NUM_PROC - 2){
			sensorNode[i].reset(&sensorNode[i - 1], NULL, sequences, i+1);
		}
		else{
			sensorNode[i].reset(&sensorNode[i - 1], &sensorNode[i+1], sequences, i+1);
		}
		//MRT
		sensorNode[i].s_status = SENSOR_OFF;
		sensorNode[i].lastAcked = 0;
		sensorNode[i].packetToSend = false;
	}

	/*Ling's code 
    for(i=1; i<NUM_PROC; i++){// set sid, nbr, and actions
        if(i==1){
			sensorNode[i-1].reset(&BS,&sensorNode[i],sequences,i);
        }
        else if(i==NUM_PROC-1){
			sensorNode[i-1].reset(&sensorNode[i-2],NULL,sequences,i);
        }else{
			sensorNode[i-1].reset(&sensorNode[i-2], &sensorNode[i],sequences,i);
        }
		//MRT
		sensorNode[i - 1].s_status = SENSOR_OFF;
		sensorNode[i - 1].lastAcked = 0;
		sensorNode[i - 1].packetToSend = false;
    }
	*/

	BS.reset(&sensorNode[0], sequences);
	BS.s_status = SENSOR_OFF;
	BS.lastAcked = 0;
	BS.packetToSend = false;

	//MRT: Computing the energy consumption by checking the code of all sensors: 
	int energy = 0;

	//for base station
	for (int i = 0; i < NUM_INST; i++){
		OPS ins = BS.s_sequences[i];
		if (ins != SLEEP){
			energy += 1;
		}
	}

	//for rest of sensors
	for (n = 0; n<NUM_PROC - 1; n++){
		for (int i = 0; i < NUM_INST; i++){
			OPS ins = sensorNode[n].s_sequences[i];
			if (ins != SLEEP){
				energy += 1; 
			}
		}
	}


	//Execute Processes Synchronusly
    for(i=0; i<NUM_LOOP; i++){
		
        for(j=0; j<NUM_INST;j++){

			//MRT
			BS.set_m_PC(j);
			for (n = 0; n<NUM_PROC - 1; n++){
				sensorNode[n].resetCollision();
				sensorNode[n].set_m_PC(j);
			}


            //execute listen and sleep and collision
            bool ls_result = BS.execute_listen_sleep();
            for(n=0; n<NUM_PROC-1; n++){
				ls_result = sensorNode[n].execute_listen_sleep();
                sensorNode[n].check_collision();
				//if (j == NUM_INST-1) cout << n << " " << sensorNode[n].collision << endl;
            }

            //execute transmit
			
			int reuslt = BS.execute_transmit();
			//if (total_transmited > 0) cout << "result= " << reuslt << endl;
			total_transmited += reuslt;
            for(n=0; n<NUM_PROC-1; n++){
				total_transmited += sensorNode[n].execute_transmit();
            }

			/* original place, I move it up and replace j +1 with j
			// reset collision variable, Update the pc in each process
			BS.set_m_PC(j + 1);
			for(n=0; n<NUM_PROC-1; n++){
                sensorNode[n].resetCollision();
				sensorNode[n].set_m_PC(j + 1);
            }
			*/

        }
        // after a loop, set PC to zero
        BS.reset();

        for(n=0; n<NUM_PROC-1; n++){
            sensorNode[n].reset();
        }
    }
    
    // Assign the objective value
	

	if (energy != 0) objective.push_back((double)total_transmited/energy);//first objective
	//else objective.push_back(0);
	objective.push_back(total_transmited);
	//objective.push_back(100 * total_transmited / (NUM_LOOP * NUM_PROC) + (NUM_PROC * NUM_INST - energy) / (NUM_PROC * NUM_INST));//first objective
	//objective.push_back(NUM_PROC * NUM_INST - energy);//second objective
    //objective.push_back(0);//third objective
	
    individual->setFitness(objective);
   
    
	if (DEBUG_OUT_ALL == 1){
        outfile<<endl<<"-----------Print Each individual------------"<<endl;
        BS.printSequences();
        for(n=0; n<NUM_PROC-1; n++){
            sensorNode[n].printSequences();
        }
        outfile<<"Objective:[";
        for(n=0; n<NUM_OBJECTIVE; n++){
            outfile<<objective[n]<<",  ";
        }
        outfile<<"]"<<endl;
		outfile << "total_transmited= " << total_transmited << endl; 
	}
    
	
}

