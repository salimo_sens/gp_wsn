/* This file contains the variable and function declarations */

# ifndef _GLOBAL_H_
# define _GLOBAL_H_

# define INF 1.0e14
# define EPS 1.0e-14
# define E  2.71828182845905
# define PI 3.14159265358979
# define GNUPLOT_COMMAND "gnuplot -persist"


#include <vector>
using namespace std;

//------------------------------------------------------------------------------+
// Constants
//------------------------------------------------------------------------------
static const int NUM_PROC = 5; // the total number of sensors including base station
static const int NUM_LOOP=5;  // # of the rounds running during the simulation
const int NUM_INST = 5;    // number of instructions per loop
const int DEBUG_OUT_ALL = 1; // output all the programs
static const int NUM_OBJECTIVE = 2; // # of objective


//------------------------------------------------------------------------------
// Helper for GA Implementation
//------------------------------------------------------------------------------
typedef struct lists
{
    int index;
    struct lists *parent;
    struct lists *child;
}
list;

int check_dominance (vector<double> a, vector<double> b, int nobj);
void insert (list *node, int x);
list* del (list *node);

# endif
