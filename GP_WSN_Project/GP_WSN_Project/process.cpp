//
//  process.cpp
//
//------------------------------------------------------------------------------
// Includes
//-----------------------------------------------------------------------------

#include "process.h"
extern std::ofstream outfile;

//------------------------------------------------------------------------------
// Helper functions
//------------------------------------------------------------------------------
static int inline getRandomInt(int min, int max)
{  // outfile<<"Process.cpp"<<endl;
    return min + (rand() % (int)(max - min + 1));
}


//------------------------------------------------------------------------------
// Abstract_Sensor  Class Implementation
//------------------------------------------------------------------------------
// 
OPS Abstract_Sensor::get_cur_action(){
	if (m_pc >= NUM_INST) 
		return NUM_ACTIONS;
	return s_sequences[m_pc]; 
}
// set the action sequences
void Abstract_Sensor::setSequences(const std::vector<int> &sequences)
{
    //MRT: sequences is for all processes, we take the part for this process using s_id. 
	// location in single sequence list
    int si = s_id*NUM_INST;
    
    // set the sequence for the process
    for (int a = 0; a < NUM_INST; ++a,++si)
    {
        s_sequences[a] = (OPS)sequences[si];
        
    }
}
//------------------------------------------------------------------------------
// NormalNode Process Class Implementation
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// - n0 : neighbor 0
// - n1 : neighbor 1
void NormalNode::setNeighbors(Abstract_Sensor *n0, NormalNode *n1)
{
    // initialize neighbors
    s_neighbor0 = n0;
    s_neighbor1 = n1;
}

//------------------------------------------------------------------------------
void NormalNode::reset(Abstract_Sensor *n0, NormalNode *n1, const std::vector<int> &sequences, int sensor_id)
{
    set_Sid(sensor_id);
    
    // reset process data
    resetCollision();
    setSequences(sequences);
    
    // reset system data
    setNeighbors(n0, n1);


   
}


////------------------------------------------------------------------------------
//void NormalNode::reset(const std::vector<int> &sequences)
//{
//    // reset process data
//    resetCollision();
//    setSequences(sequences);
//}

//------------------------------------------------------------------------------
void NormalNode::reset()
{
    // reset process data
    resetCollision();
	set_m_PC(0);

}

//------------------------------------------------------------------------------
// execute the current action if current action is sleep or listen
int NormalNode::execute_listen_sleep()  //return -1 to denote an invalid program, return 0 to denote false, and 1 to denote true. 
{ 
	m_pc = get_m_PC();
    if(m_pc>=NUM_INST) return 0;
    OPS action_num=s_sequences[m_pc];
	bool execute = false;

    switch (action_num) {
        case LISTEN:
			execute = true;
            s_listen();
            break;
        case SLEEP:
			execute = true;
            s_sleep();
            break;
        case TRANSMIT:
            //should do nothing for now
			set_Status(SENSOR_OFF);
            break;
        default:
            return 0;
            break;
    }
	
	//cout << get_Sid() << "- In execute_listen_sleep: " << execute << ", m_pc:" << m_pc << endl;
	return (execute) ? 1 : 0;
}


//------------------------------------------------------------------------------
// execute the current action if current action is sleep or listen
int NormalNode::execute_transmit()   //MRT: -2 is old false, -1 means program is not valid, anything else is number of transmited messages sucessfully 
{   
	int transmited = 0; //total number fo sucessful transmitions. It is either 0, 1, or 2. 
	m_pc = get_m_PC();
    if(m_pc>=NUM_INST) return 0;
    OPS action_num=s_sequences[m_pc];
	bool executed = false;
    
    switch (action_num) {
        case LISTEN:
            //already taken care of
            break;
        case SLEEP:
            //already taken care of
            break;
        case TRANSMIT:
			executed = true;

			if (s_neighbor0 != NULL && !s_neighbor0->collision){
				transmited += s_transmit_left(s_neighbor0);
			}
			if (s_neighbor1 != NULL && !s_neighbor1->collision){
				transmited += s_transmit_right(s_neighbor1);
			}
            break;
        default:
            break;
    }

	//cout << get_Sid() << "- In execute_transmit: " << executed <<", m_pc:"<<m_pc << endl;
	return transmited; //MRT: If transmition executed, we return the reuslt, otherwise we return -2 to indicate there was not no transmition. 
}



//------------------------------------------------------------------------------
//send message to nbr
int NormalNode::s_transmit_left(Abstract_Sensor *nbr){
    int sender_sid=get_Sid();
	set_Status(SENDING);
	
	//Nothing to send
	if (!packetToSend) return 0;

    if(nbr->get_Status()==SENSOR_ON){
        // sending a message to sensor nbr
		//MRT: mean I am wainting for ack from the neighbor.

		/* old//---
		if (get_receive(nbr->get_Sid()) > 0)  dec_receive(nbr->get_Sid());
		else inc_ack(nbr->get_Sid());// increase the ack for nbr node


		if (nbr->get_ack(sender_sid)>0){
			nbr->dec_ack(sender_sid);
		}
		else
		{
			nbr->inc_receive(get_Sid());// increase the receive record of nbr
		}*/ 


			int messageNumber = lastAcked + 1; 
			
			if (nbr->lastAcked + 1 == messageNumber)
			{
				nbr->packetToSend = false;
				nbr->lastAcked++;
				if (s_neighbor1 == NULL) {
					lastAcked++; //if the right neibor is NULL, we don't wait for ack from it. 
					packetToSend = false;
				}
				return 1;
			}
			/*
		else {
			int messageNumber = lastAcked + 1; 
			if (nbr->lastAcked + 1 == messageNumber && (nbr->packetToSend == false || ) {
				nbr->packetToSend = true;
				return 1; //score for fitness
			}
			//otherwise discar; doing nothing
		}*/
    }
	return 0;
}

int NormalNode::s_transmit_right(NormalNode *nbr){
	int sender_sid = get_Sid();
	set_Status(SENDING);

	//Nothing to send
	if (!packetToSend) return 0;

	if (nbr->get_Status() == SENSOR_ON){
		// sending a message to sensor nbr
		//MRT: mean I am wainting for ack from the neighbor.

		/* old//---
		if (get_receive(nbr->get_Sid()) > 0)  dec_receive(nbr->get_Sid());
		else inc_ack(nbr->get_Sid());// increase the ack for nbr node


		if (nbr->get_ack(sender_sid)>0){
		nbr->dec_ack(sender_sid);
		}
		else
		{
		nbr->inc_receive(get_Sid());// increase the receive record of nbr
		}*/



		int messageNumber = lastAcked + 1;
		if (nbr->lastAcked + 1 == messageNumber && nbr->packetToSend == false) {
		nbr->packetToSend = true;
		return 1; //score for fitness
		}
	}
	return 0;
}

//------------------------------------------------------------------------------
// Check if there is collision at the sensor
bool NormalNode::check_collision(){

	//if (s_neighbor1 != NULL) cout  << s_id  << ": neighbor1  is not NULL" << endl;
    if (s_neighbor0 != NULL && s_neighbor1 != NULL){
		//cout << s_neighbor0->get_cur_action() << " " << s_neighbor1->get_cur_action() << endl;
        if(s_neighbor0->get_cur_action()==TRANSMIT && s_neighbor1->get_cur_action()==TRANSMIT){
            collision=true;
        }
    }
    return collision;
}

//------------------------------------------------------------------------------
// print the valid part of a process's sequence
void NormalNode::printSequences()
{
    outfile<<"SENSOR "<<get_Sid()<<"     : ";
	
	for (int action = 0; action < NUM_INST; ++action)
    {
       outfile<<ACTION_OPS_NAMES[s_sequences[action]]<<",  ";
    }
    outfile<<endl;
}





//------------------------------------------------------------------------------
// SinkNode Process Class Implementation
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void SinkNode::setNeighbors(NormalNode *n0)
{
    // initialize neighbors
    s_neighbor0 = n0;
}

void SinkNode::reset(NormalNode *n0, const std::vector<int> &sequences){
	// reset process data
	resetCollision();
	setSequences(sequences);

	// reset system data
	setNeighbors(n0);

}

//------------------------------------------------------------------------------
void SinkNode::reset()
{
    // reset process data
    resetCollision();
	set_m_PC(0);

}


//------------------------------------------------------------------------------
// execute the current action if current action is sleep or listen
int SinkNode::execute_listen_sleep()
{
	m_pc = get_m_PC();
    if(m_pc>=NUM_INST) return false;
	bool execute = false;
    OPS action_num=s_sequences[m_pc];
    
    switch (action_num) {
		case LISTEN:
            s_listen();
			execute = true;
            break;
        case SLEEP:
            s_sleep();
			execute = true;
            break;
        case TRANSMIT:
            // will consider in  function execute_transmit
			set_Status(SENSOR_OFF);
            break;
        default:
            return false;
            break;
    }

	//cout << get_Sid() << "- In execute_listen_sleep: " << execute << ", m_pc:" << m_pc << endl;
	return (execute) ?  1 : 0;
}


//------------------------------------------------------------------------------
// execute the current action if current action is sending
int SinkNode::execute_transmit()
{
	int transmited = 0;

	m_pc = get_m_PC();
	if (m_pc >= NUM_INST) return 0;
	OPS action_num = s_sequences[m_pc];
	bool executed = false;

	switch (action_num) {
	case LISTEN:
		//already taken care of
		break;
	case SLEEP:
		//already taken care of
		break;
	case TRANSMIT:
		executed = true;
		if (s_neighbor0 != NULL && !s_neighbor0->collision) transmited += s_transmit(s_neighbor0);
		break;
	default:
		break;
	}

	//cout << get_Sid() << "- In execute_transmit: " << executed << ", m_pc:" << m_pc << endl;
	return transmited;
}


//------------------------------------------------------------------------------
//send message to nbr
int SinkNode::s_transmit(NormalNode *nbr){
    int sender_sid=get_Sid();
	set_Status(SENDING);
    if(nbr->get_Status()==SENSOR_ON){
        // sending a message to sensor nbr

		/* old
		if (get_receive(nbr->get_Sid()) > 0)  dec_receive(nbr->get_Sid());
        else inc_ack(nbr->get_Sid());// increase the ack for nbr node
       
        
        if(nbr->get_ack(sender_sid)>0){
            nbr->dec_ack(sender_sid);
        }
		else
		{
			nbr->inc_receive(get_Sid());// increase the receive record of nbr
		}
		return 1;
		*/

		int messageNumber = lastAcked + 1;
		//cout << "lastAck= " << lastAcked << " nbr->lastAck= " << nbr->lastAcked  << endl;
		if (nbr->lastAcked + 1 == messageNumber && nbr->packetToSend == false)
		{
			nbr->packetToSend = true;
			return 1; //score for fitness

		}

    }
	return 0;
}

//------------------------------------------------------------------------------
// print the valid part of a process's sequence
void SinkNode::printSequences()
{
    outfile<<"BASE STATION"<<": ";
	for (int action = 0; action < NUM_INST; ++action)
    {
        outfile<<ACTION_OPS_NAMES[s_sequences[action]]<<",  ";
    }
    outfile<<endl;
}















