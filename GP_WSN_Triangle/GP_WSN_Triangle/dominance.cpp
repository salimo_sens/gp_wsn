/* Domination checking routines */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "global.h"
#include "rand.h"

/* Routine for usual non-domination checking
   It will return the following values
   1 if a dominates b
   -1 if b dominates a
   0 if both a and b are non-dominated */

int check_dominance (vector<double> a, vector<double> b, int nobj)
{
    int i;
    int flag1;
    int flag2;
    flag1 = 0;
    flag2 = 0;
    
    
    for (i=0; i<nobj; i++)
    {
        if (a[i] > b[i])
        {
            flag1 = 1;
            
        }
        else
        {
            if (a[i] < b[i])
            {
                flag2 = 1;
            }
        }
    }
    if (flag1==1 && flag2==0)
    {
        return (1);
    }
    else
    {
        if (flag1==0 && flag2==1)
        {
            return (-1);
        }
        else
        {
            return (0);
        }
    }
    


}
