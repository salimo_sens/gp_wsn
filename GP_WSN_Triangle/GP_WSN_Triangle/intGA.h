//
//  intGA.h
//  
//

#ifndef NSGAII_intGA_h
#define NSGAII_intGA_h

//------------------------------------------------------------------------------
// Includes
//------------------------------------------------------------------------------
#include <vector>
#include <time.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "global.h"

using std::vector;
using namespace std;

//------------------------------------------------------------------------------
// Prototypes
//------------------------------------------------------------------------------
double getRandomGaussian();

//------------------------------------------------------------------------------
static int inline getRandomInt(int min, int max)
{ 
    return min + (rand() % (int)(max - min + 1));
}

//------------------------------------------------------------------------------
static double inline getRandomUniform()
{
    return ((double)rand() / RAND_MAX);
}



//------------------------------------------------------------------------------
// Genotype
//------------------------------------------------------------------------------
class Int_IND
{
    
protected:
	int m_numGenes;             // number of genes
	vector<double> m_fitness;   // fitness of this genotype
	vector<int> m_genes;        // weights
    bool m_evaluated;           // is the fitness valid
    bool m_maximize;            // maximizing or minimizing fitness

    int rank;               // change to private later
    double crowd_dist;      // change to p
    
public:

    
    Int_IND(int num_genes, bool maximize);
    
    // manipulate genes
    void mutateGenes(double rate, int min, int max);
    void setGenes(const vector<int> &new_genes);
    void randomizeGenes(int min, int max);
    const vector<int>& getGenes() const { return m_genes; }
    
    // create new individual using crossover
    void crossoverSP(const Int_IND *other, Int_IND *newUI_1, Int_IND *newUI_2);
    
    // manipulate fitness
    void setFitness(const vector<double> &fit);
    const vector<double> getFitness();
    double getOneFitness(int i){return m_fitness[i];};
    
    void setRank(int i){rank = i;};
    int getRank(){return rank;};
    
    void set_crowd_dist(double i){crowd_dist = i;};
    double get_crowd_dist(){return crowd_dist;};
    
    void copy_from(Int_IND *newUI);
    bool isEvaluated() { return m_evaluated; }
    int domination_check(vector<double> b);
    
    // overload the comparison operator
    //bool operator<(const Int_IND &rhs) const;
};



//------------------------------------------------------------------------------
// Int_GA
//------------------------------------------------------------------------------
struct PopulationInfo {
    
    // number of individuals in population
    int pop_size;
    
    // subpopulation size (used for tournament selection)
    int sub_size;
    
    // number of genes per individual
    int genome_length;
    
    // maximum value for each gene
    int gene_max;
    
    // minimum value for each gene
    int gene_min;
    
    // number of evolutionary generations
    int num_generations;
    
    // number of individuals to compete during tournaments
    int tournament_size;
    
    // does the population contain elites
    bool elitism;
    
    // probability of mutation during individual reproduction
    double mutation_rate;
        
    // will the GA maxmize or minimize fitness
    bool maximize;
    
    // function used to calculate individuals fitness
    void (*calculate_fitness)(Int_IND*);
};

class Int_GA {
    
protected:
    
    // genetic algorithm parameters
    int m_popSize;      // number of individuals
    int m_subSize;      // number of individuals in overlapping subpopulations
    int m_tournSize;    // size of selection tournament
	int m_numGen;       // number of generations to run
    int m_curGen;       // current generation count
    bool m_elitism;     // keep the best individual(s)
    vector<Int_IND*> archive_individuals;
    vector<Int_IND*> m_individuals;
    bool maximize;

    // individual manipulation parameters
    int m_genomeSize;   // number of genes per individual
    int m_geneMax;      // maximum value for each locus
    int m_geneMin;      // minimum value for each locus
    double m_muteProb;  // gene mutation probability
	
    // fitness evaluation callback
    void (*m_fitnessFunc)(Int_IND*);
    
private: 
    // used to select parents
    Int_IND* tournament (Int_IND* ind1, Int_IND* ind2);
    
public:
    Int_GA(const PopulationInfo& info);
    ~Int_GA();
    
    // population manipulation
    bool nextGen();
    void reproduce();
    void evaluate();
    void selection();
    
    void merge_pop(vector<Int_IND*> mix_population);
    void fill_nondominated_sort (vector<Int_IND*> mixed_pop);
    void crowding_fill(vector<Int_IND*> mixed_pop, vector<Int_IND*> new_pop, int count, int front_size, list *elite);
    
    void assign_rank_and_crowding_distance (vector<Int_IND*> m_pop);
    void assign_crowding_distance_list(vector<Int_IND*> m_pop, list *lst, int front_size);
    void assign_crowding_distance_indices (vector<Int_IND*> m_pop, int c1, int c2);
    void assign_crowding_distance (vector<Int_IND*> m_pop, int *dist, int **obj_array, int front_size);
    
    void quicksort_front_obj(vector<Int_IND*> m_pop, int objcount, int obj_array[], int obj_array_size);
    void q_sort_front_obj(vector<Int_IND*> m_pop, int objcount, int obj_array[], int left, int right);
    void quicksort_dist(vector<Int_IND*> m_pop, int *dist, int front_size);
    void q_sort_dist(vector<Int_IND*> m_pop, int *dist, int left, int right);
    
    bool isComplete() { return (m_curGen >= m_numGen); }
	void print_Gnomes();
    
    int getCurGen() { return m_curGen; }
};


#endif
