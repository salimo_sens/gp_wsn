//
//  process.h
//
// This file contains the declaration of sensor networks(decoded from Genome)
//

#ifndef process_h
#define process_h

//------------------------------------------------------------------------------
// Includes
//------------------------------------------------------------------------------
#include <string>
#include <cstdlib>
#include <vector>
#include <iostream>
#include <fstream>
#include "global.h"
using namespace std;

//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Helper Structs
//------------------------------------------------------------------

// actions
enum ACTION_OPS
{
    TRANSMIT,       //Action: sensor transmit
    LISTEN,			//sensor Listen
    SLEEP,          //sensor Sleep
    NUM_ACTIONS
};

// Name of action, this is used for printing the actions
const std::string ACTION_OPS_NAMES[NUM_ACTIONS] =
{
    "TRANSMIT",
    "LISTEN",
    "SLEEP"
};

// Sensor status
enum SENSOR_STATUS
{
    SENSOR_OFF,     //sensor sleep
    SENSOR_ON,		//sensor Listen
    SENDING,        //sensor sending
    NUM_STATUS
};

typedef ACTION_OPS OPS;





//------------------------------------------------------------------------------
// Sensor  Class Definition
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// abstract class for all sensors
class Abstract_Sensor
{
protected:
    int m_pc;   // process counter (points to next action)
    
	int s_id;    // ID
    void setSequences(const std::vector<int> &sequences);//
    
public:
	vector<OPS> s_sequences;// sequence of actions

	SENSOR_STATUS s_status;// Status of sensor
	
	vector<int> ack_record;
	vector<int> recieve_record;

	//MRT
	bool packetToSend;
	int lastAcked;
	bool collision;
   
public:
	int get_Status(){ return s_status; }; // get sensor status
	void set_Status(SENSOR_STATUS i){ s_status = i; };// set sensor ID
    
	int get_Sid(){ return s_id; };    //get sensor ID
	void set_Sid(int i){ s_id = i; };  //get sensor ID
    
    bool s_sleep(){set_Status(SENSOR_OFF);return true;}; // Sensor sleep and set the status to off
    bool s_listen(){set_Status(SENSOR_ON);return true;};// Sensor start to listen and set the status to On
   
	void inc_ack(int receiver_id){ ack_record[receiver_id]++; };
	void dec_ack(int receiver_id){ ack_record[receiver_id]--; };
	void inc_receive(int sender_id){ recieve_record[sender_id]++; };
	void dec_receive(int sender_id){ recieve_record[sender_id]--; };
	int get_ack(int receiver_id){ return ack_record[receiver_id]; };
	int get_receive(int receiver_id){ return recieve_record[receiver_id]; };

    void resetCollision(){collision=false;}// reset collision status
	OPS get_cur_action();//get current action
	int get_m_PC(){ return m_pc; };//get program counter
	void set_m_PC(int i){ m_pc = i; };//set Program Counter

	Abstract_Sensor(int sensor_id) : s_id(sensor_id), s_status(SENSOR_OFF), collision(false), lastAcked(0), packetToSend(false) {
		for (int i = 0; i<NUM_PROC; i++){
			ack_record.push_back(0);
			recieve_record.push_back(0);
		}
		set_m_PC(0);
	};
	Abstract_Sensor() : s_status(SENSOR_OFF), collision(false){
		for (int i = 0; i<NUM_PROC; i++){
			ack_record.push_back(0);
			recieve_record.push_back(0);
		}
		set_m_PC(0);
	};

	
};

//------------------------------------------------------------------------------
// Regular Sensor Node class
class NormalNode : public Abstract_Sensor
{
protected:


    void setNeighbors(Abstract_Sensor *n0, NormalNode *n1);//
   
    
public:
	// pointers to neighbors and sink
	Abstract_Sensor *s_neighbor0;     // first neighbor
	NormalNode *s_neighbor1;     // second neighbor

	NormalNode(int i) : Abstract_Sensor(i)
    {
        s_sequences.resize(NUM_INST);
    }
    
	NormalNode() : Abstract_Sensor()
    {
        s_sequences.resize(NUM_INST);
    }
    
    // Sensor action
    int s_transmit_left(Abstract_Sensor *nbr); // sensor sending message
	int s_transmit_right(NormalNode *nbr);


    // reset process
    void reset(Abstract_Sensor *n0, NormalNode *n1, const std::vector<int> &sequences, int sensor_id);//
//    void reset(Abstract_Sensor *n0, Abstract_Sensor *n1);
//    void reset(const std::vector<int> &sequences);
    void reset();
    
    // execute the action
    int execute_listen_sleep();
    int execute_transmit();
    bool check_collision();
    
    // helper utilities
    void printSequences();

	
};

//------------------------------------------------------------------------------
// Base Station class
class SinkNode : public Abstract_Sensor
{
protected:
    // process data
    int m_pc;               // process counter (points to next action)
    
    // pointers to neighbor
    NormalNode *s_neighbor0;     // first neighbor
    void setNeighbors(NormalNode *n0);

    
public:
    SinkNode() : Abstract_Sensor(0)
    {
        s_sequences.resize(NUM_INST);
    }
    
    // Sensor actions
    int s_transmit(NormalNode *nbr); // sensor sending message
    
    // execute the action
    int execute_listen_sleep();
    int execute_transmit();
	void reset(NormalNode *n0, const std::vector<int> &sequences);//
    void reset();
	
    
    // helper utilities
    void printSequences();

};

#endif

























